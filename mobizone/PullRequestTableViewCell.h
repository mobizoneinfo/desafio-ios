//
//  PullRequestTableViewCell.h
//  mobizone
//
//  Created by Intermat on 05/01/17.
//  Copyright © 2017 Mobizone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullRequestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *detail;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *fullName;

@end
