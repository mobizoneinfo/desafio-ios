//
//  User.h
//
//  Created by   on 05/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface User : NSObject 

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *avatarUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;


@end
