//
//  BaseClass.m
//
//  Created by   on 04/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Repository.h"
#import "Items.h"


NSString *const kBaseClassTotalCount = @"total_count";
NSString *const kBaseClassIncompleteResults = @"incomplete_results";
NSString *const kBaseClassItems = @"items";


@interface Repository ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Repository

@synthesize totalCount = _totalCount;
@synthesize incompleteResults = _incompleteResults;
@synthesize items = _items;


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.totalCount = [[self objectOrNilForKey:kBaseClassTotalCount fromDictionary:dict] doubleValue];
            self.incompleteResults = [[self objectOrNilForKey:kBaseClassIncompleteResults fromDictionary:dict] boolValue];
    NSObject *receivedItems = [dict objectForKey:kBaseClassItems];
    NSMutableArray *parsedItems = [NSMutableArray array];
    if ([receivedItems isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedItems) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedItems addObject:[Items modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedItems isKindOfClass:[NSDictionary class]]) {
       [parsedItems addObject:[Items modelObjectWithDictionary:(NSDictionary *)receivedItems]];
    }

    self.items = [NSArray arrayWithArray:parsedItems];

    }
    
    return self;
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods



@end
