//
//  RepositoriesTableViewController.m
//  mobizone
//
//  Created by Intermat on 04/01/17.
//  Copyright © 2017 Mobizone. All rights reserved.
//

#import "RepositoriesTableViewController.h"
#import "AFNetworking.h"
#import "RepoTableViewCell.h"
#import "Repository.h"
#import "Items.h"
#import "Owner.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PullRequestTableViewController.h"
#import "UIScrollView+InfiniteScroll.h"


@interface RepositoriesTableViewController (){
    
    
    NSNumber* actualPage;
    NSMutableArray *arrData;
}



@end

@implementation RepositoriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    actualPage = @1;
    arrData = [[NSMutableArray alloc] init];
    
    __weak typeof(self) weakSelf = self;
    
    // setup infinite scroll
    [self.tableView addInfiniteScrollWithHandler:^(UITableView* tableView) {
        // update table view
        [weakSelf loadData];
        
        // finish infinite scroll animation
        [tableView finishInfiniteScroll];
    }];
    
    
    //[self.tableView beginInfiniteScroll:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self loadData];
    
}


-(void)loadData {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%@", actualPage]];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            Repository *base = [[Repository alloc] initWithDictionary:responseObject];
            [arrData addObjectsFromArray:base.items];
            actualPage = [NSNumber numberWithInt:[actualPage integerValue] + 1];
            
            [self.tableView reloadData];
        }
        
        
    }];
    [dataTask resume];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrData count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString* simpleTableIdentifier = @"cellID";
    
    RepoTableViewCell *cell = (RepoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RepoTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    Items* item = arrData[indexPath.row];
    
    cell.repository.text =  item.name;
    cell.descriptionLabel.text = item.itemsDescription;
    cell.userName.text  = item.owner.login;
    
    cell.name.text = item.fullName;
    cell.stars.text = [NSString stringWithFormat:@"%.f" , item.stargazersCount];
    cell.fork.text = [NSString stringWithFormat:@"%.f" , item.forksCount];
    
    
    cell.avatar.layer.borderWidth = 0;
    cell.avatar.layer.masksToBounds = YES;
    cell.avatar.layer.cornerRadius = cell.avatar.frame.size.height /2;
    
    [cell.avatar sd_setImageWithURL:[NSURL URLWithString:item.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"next" sender:nil];
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    PullRequestTableViewController *pull = segue.destinationViewController;
    
    Items* item = arrData[indexPath.row];
    
    pull.urlPull = [NSString stringWithFormat:@"https://api.github.com/repos/%@/pulls", item.fullName];
    pull.title = item.name;
    
}


@end
