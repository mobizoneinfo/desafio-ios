//
//  Items.h
//
//  Created by   on 04/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Owner;

@interface Items : NSObject 


@property (nonatomic, strong) Owner *owner;

@property (nonatomic, strong) NSString *itemsDescription;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, assign) double forksCount;

@property (nonatomic, strong) NSString *fullName;

@property (nonatomic, assign) double stargazersCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;


@end
