//
//  PullRequests.m
//
//  Created by   on 05/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PullRequests.h"
#import "User.h"



NSString *const kPullRequestsMilestone = @"milestone";
NSString *const kPullRequestsLocked = @"locked";
NSString *const kPullRequestsTitle = @"title";
NSString *const kPullRequestsUrl = @"url";
NSString *const kPullRequestsCommitsUrl = @"commits_url";
NSString *const kPullRequestsMergeCommitSha = @"merge_commit_sha";
NSString *const kPullRequestsReviewCommentUrl = @"review_comment_url";
NSString *const kPullRequestsUpdatedAt = @"updated_at";
NSString *const kPullRequestsBase = @"base";
NSString *const kPullRequestsReviewCommentsUrl = @"review_comments_url";
NSString *const kPullRequestsAssignee = @"assignee";
NSString *const kPullRequestsCommentsUrl = @"comments_url";
NSString *const kPullRequestsPatchUrl = @"patch_url";
NSString *const kPullRequestsMergedAt = @"merged_at";
NSString *const kPullRequestsState = @"state";
NSString *const kPullRequestsBody = @"body";
NSString *const kPullRequestsId = @"id";
NSString *const kPullRequestsNumber = @"number";
NSString *const kPullRequestsIssueUrl = @"issue_url";
NSString *const kPullRequestsUser = @"user";
NSString *const kPullRequestsClosedAt = @"closed_at";
NSString *const kPullRequestsHead = @"head";
NSString *const kPullRequestsAssignees = @"assignees";
NSString *const kPullRequestsStatusesUrl = @"statuses_url";
NSString *const kPullRequestsCreatedAt = @"created_at";
NSString *const kPullRequestsLinks = @"_links";
NSString *const kPullRequestsDiffUrl = @"diff_url";
NSString *const kPullRequestsHtmlUrl = @"html_url";


@interface PullRequests ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PullRequests

@synthesize title = _title;
@synthesize body = _body;
@synthesize user = _user;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {

            self.title = [self objectOrNilForKey:kPullRequestsTitle fromDictionary:dict];
            self.body = [self objectOrNilForKey:kPullRequestsBody fromDictionary:dict];
            self.user = [User modelObjectWithDictionary:[dict objectForKey:kPullRequestsUser]];


    }
    
    return self;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}



@end
