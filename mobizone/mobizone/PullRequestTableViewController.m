//
//  PullRequestTableViewController.m
//  mobizone
//
//  Created by Intermat on 05/01/17.
//  Copyright © 2017 Mobizone. All rights reserved.
//

#import "PullRequestTableViewController.h"
#import "PullRequestTableViewCell.h"
#import "AFNetworking.h"
#import "PullRequests.h"
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PullRequestTableViewController (){
    PullRequests *baseClass;
    
    NSMutableArray* arrData;
}

@end

@implementation PullRequestTableViewController

@synthesize urlPull;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [[NSMutableArray alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}




-(void)viewWillAppear:(BOOL)animated{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:self.urlPull];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            
            for (NSDictionary *dic in responseObject) {
                
                baseClass = [[PullRequests alloc] initWithDictionary:dic];
                
                [arrData addObject:baseClass];
            }
            
            [self.tableView reloadData];
        }
        
        
    }];
    [dataTask resume];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    
    static NSString* simpleTableIdentifier = @"pullCellID";
    
    PullRequestTableViewCell *cell = (PullRequestTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PullRequestTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    PullRequests* item = arrData[indexPath.row];
    
    cell.detail.text =  item.body;
    cell.title.text = item.title;
    cell.username.text  = item.user.login;
    cell.fullName.text = @"";
   
    cell.avatar.layer.borderWidth = 0;
    cell.avatar.layer.masksToBounds = YES;
    cell.avatar.layer.cornerRadius = cell.avatar.frame.size.height /2;

    
    [cell.avatar sd_setImageWithURL:[NSURL URLWithString:item.user.avatarUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    return cell;

    
}


@end
