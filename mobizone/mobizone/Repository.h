//
//  BaseClass.h
//
//  Created by   on 04/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Repository : NSObject 

@property (nonatomic, assign) double totalCount;
@property (nonatomic, assign) BOOL incompleteResults;
@property (nonatomic, strong) NSArray *items;
- (instancetype)initWithDictionary:(NSDictionary *)dict;


@end
