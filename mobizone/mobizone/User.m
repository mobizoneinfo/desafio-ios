//
//  User.m
//
//  Created by   on 05/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "User.h"


NSString *const kUserLogin = @"login";
NSString *const kUserAvatarUrl = @"avatar_url";


@interface User ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation User

@synthesize login = _login;
@synthesize avatarUrl = _avatarUrl;



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.login = [self objectOrNilForKey:kUserLogin fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kUserAvatarUrl fromDictionary:dict];

    }
    
    return self;
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
