//
//  Items.m
//
//  Created by   on 04/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Items.h"
#import "Owner.h"

NSString *const kItemsOwner = @"owner";
NSString *const kItemsDescription = @"description";
NSString *const kItemsName = @"name";
NSString *const kItemsForksCount = @"forks_count";
NSString *const kItemsFullName = @"full_name";
NSString *const kItemsStargazersCount = @"stargazers_count";


@interface Items ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Items


@synthesize owner = _owner;
@synthesize itemsDescription = _itemsDescription;
@synthesize name = _name;
@synthesize forksCount = _forksCount;
@synthesize fullName = _fullName;
@synthesize stargazersCount = _stargazersCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {

            self.owner = [Owner modelObjectWithDictionary:[dict objectForKey:kItemsOwner]];
            self.itemsDescription = [self objectOrNilForKey:kItemsDescription fromDictionary:dict];
            self.name = [self objectOrNilForKey:kItemsName fromDictionary:dict];
            self.forksCount = [[self objectOrNilForKey:kItemsForksCount fromDictionary:dict] doubleValue];
            self.fullName = [self objectOrNilForKey:kItemsFullName fromDictionary:dict];
            self.stargazersCount = [[self objectOrNilForKey:kItemsStargazersCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}



#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
